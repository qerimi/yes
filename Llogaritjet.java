public class Llogaritjet
{
   public static double[] mbledhja(double x1, double x2, double y1, double y2)
   {    
      double a = x1 + x2; 
      double b = y1 + y2;
	   
      return "("+a+")+("+b+")i";
   }
 
   public static double[] zbritja(double x1, double x2, double y1, double y2)
   {    
      double a = x1 - x2; 
      double b = y1 - y2;
	   
      return "("+a+")+("+b+")i";
   }
}