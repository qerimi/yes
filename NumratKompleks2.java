import javax.swing.*;

public class NumratKompleks2 extends JPanel
{	
		public static void main (String[] args)
		{ 				
         String X1 = JOptionPane.showInputDialog("Shtyp pjesen reale te numrit kompleks z1:");
			   double x1 = new Double(X1).doubleValue();
			String Y1 = JOptionPane.showInputDialog("Shtyp pjesen imagjinare te numrit kompleks z1:");
		      double y1 = new Double(Y1).doubleValue();
		   
		   String X2 = JOptionPane.showInputDialog("Shtyp pjesen reale te numrit kompleks z2:");
			   double x2 = new Double(X2).doubleValue();
			String Y2 = JOptionPane.showInputDialog("Shtyp pjesen imagjinare te numrit kompleks z2:");
		      double y2 = new Double(Y2).doubleValue();
		    
		   JOptionPane.showMessageDialog(null, "Numri i pare kompleks: \n z1=("+x1+")+("+y1+")i");
		   JOptionPane.showMessageDialog(null, "Numri i dyte kompleks: \n z2=("+x2+")+("+y2+")i");
		    
		      String shuma = Llogaritjet.mbledhja(x1, x2, y1, y2);
		      String ndryshimi = Llogaritjet.zbritja(x1, x2, y1, y2);
		   
		   JOptionPane.showMessageDialog(null, "Shuma: \n z= " +"shuma"-;
		   JOptionPane.showMessageDialog(null, "Ndryshimi: \n z=("+ndryshimi[0]+")+("+ndryshimi[1]+")i");
		}
}
